#!/usr/bin/python3

# 以PIP3 安裝
# pip3 install inputs PySerial
import time
import serial
import inputs  # devices,get_gamepad
import sys
import select

#from inputs import devices,get_gamepad
# ser=serial.Serial( '/dev/ttyS0',115200,timeout=1)
# ser=serial.Serial( '/dev/ttyUSB0',115200,timeout=1)
# ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)


class Tircgo:
    TIMEOUT = 0.3
    # 開啟的 USB PORT, NONBLOCKING, 讀取等待 0.3秒
    ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=0.3)

    def __init__(self):
        print("init TICOGO")

    # 等待RS232回應, 最少收BYTE數, 要DELAY多少次, 每次DELAY多少秒(EX: 0.5*60次, 共30秒)
    def waitResponse(self, minLen=4, maxDelay=60, loopDelay=1):
        ret = b''
        time.sleep(0.1)
        # maxDelay=3
        for i in range(0, maxDelay):
            ret = self.ser.read(minLen-len(ret))
            if len(ret) >= minLen:
                return ret
            # 顯示 '.' 表示進行中, 不使用 print, 原因是會BUFFER/換行
            sys.stdout.write('.')
            sys.stdout.flush()  # 強迫顯示
            time.sleep(loopDelay)  # 每次間隔時間

        # print ("")
        return ret

    # 計算CHECKSUM, CMD為向AGV發送的訊息, 計算到倒數一BYTE

    def checkSum(self, cmd):
        sum = 0
        for v in range(0, len(cmd)-1):
            sum += cmd[v]
        return (sum & 0xff)

    CMD_GETALIVE = list(b'\xf0\x61\x00\x51')

    # 測試是否己開完機!!
    def getAlive(self):
        cmd = self.CMD_GETALIVE
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("getAlive", [hex(x) for x in cmd], sum)

        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    CMD_GET_CURRENT_MODE = list(b'\xf0\x62\x00\x51')

    # 讀取目前運作模式
    def getCurrentMode(self):
        cmd = self.CMD_GET_CURRENT_MODE
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("getCurrentMode", [hex(x) for x in cmd], sum)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300, 2)

        # 是否有錯誤訊息, 所以必需再讀一BYTE!!
        if len(ret) >= 4 and ret[2] != 0:
            if len(ret) == 4:
                x = self.ser.read(1)
                ret += x

        return ret

    CMD_GETWORK_STATUS = list(b'\xf0\x63\x00\x51')

    # 讀取工作狀態 ; 常未測試
    def getWorkingStatus(self):
        cmd = self.CMD_GETWORK_STATUS
        sum = self. checkSum(cmd)
        cmd[len(cmd)-1] = sum
        print("getWorkStatus", [hex(x) for x in cmd], sum)

        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    CMD_GETLIDAR = list(b'\xf0\x64\x00\x55')

    # 讀取LIDAR ; 尚未測試
    def getGetLidar(self):
        cmd = self.CMD_GETLIDAR
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("getGetLidar", [hex(x) for x in cmd], sum)
        self.ser.write(cmd)
        # ret = self.ser.read(5)
        ret = self.waitResponse(5, 300)
        val = b""
        ret = b'\x00\x00\x00\xa5\x00'
        # 處理長度訊息
        if len(ret) == 5 and ret[2] == 0:
            x = ret[3]
            val += (x >> 6 & 0x03) .to_bytes(1, 'big')
            val += (x >> 4 & 0x03) .to_bytes(1, 'big')
            val += (x >> 2 & 0x03) .to_bytes(1, 'big')
            val += (x >> 0 & 0x03) .to_bytes(1, 'big')
        return val

    CMD_SHUTDOWN = list(b'\xf0\x70\x00\x51')

    # 關機
    def shutdown(self):
        cmd = self.CMD_SHUTDOWN
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum
        print("shutdown", [hex(x) for x in cmd], sum)

        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    CMD_MOVE = list(b'\xf0\x72\x00\x00\00')

    # 移動... 速度<=60CM/SEC, ROTTE<=10
    def move(self, xSpeed=0, zRotate=0):
        if xSpeed >= 60:
            xSpeed = 60
        if xSpeed <= -60:
            xSpeed = -60
        xSpeed &= 0xff
        # if xSpeed<0: xSpeed= ~xSpeed

        if zRotate >= 10:
            zRotate = 10
        if zRotate <= -10:
            zRotate = -10
        zRotate &= 0xff
        # if zRotate<0: zRotate= ~ zRotate

        # 填入COMMAND
        cmd = self.CMD_MOVE
        cmd[2] = xSpeed
        cmd[3] = zRotate
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("move", [hex(x) for x in cmd], sum, xSpeed, zRotate)
        # 傳送命令
        self.ser.write(cmd)
        # s = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    CMD_DEVICE = list(b'\xf0\x74\xdd\xee\xcc\x00')

    # 發送聲音, 類別, 次數

    def beep(self, btype=1, count=1):
        if btype < 0:
            btype = 0
        if btype >= 4:
            btype = 4
        if count <= 0:
            count = 1
        if count > 10:
            count = 10

        cmd = self.CMD_DEVICE
        cmd[2] = 0x00
        cmd[3] = btype
        cmd[4] = count
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("beep", [hex(x) for x in cmd], sum, btype, count)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    def ledGreen(self, btype=1, count=1):
        if btype < 0:
            btype = 1  # OFF
        if btype >= 4:
            btype = 4
        if count <= 0:
            count = 1
        if count > 10:
            count = 10

        cmd = self.CMD_DEVICE
        cmd[2] = 0x1
        cmd[3] = btype
        cmd[4] = count
        sum = self. checkSum(cmd)
        cmd[len(cmd)-1] = sum
        print("led green", [hex(x) for x in cmd], sum, btype, count)
        self.ser.write(cmd)
        # s = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    def ledYellow(self, btype=1, count=1):
        if btype < 0:
            btype = 1  # OFF
        if btype >= 4:
            btype = 4
        if count <= 0:
            count = 1
        if count > 10:
            count = 10

        cmd = self.CMD_DEVICE
        cmd[2] = 0x2
        cmd[3] = btype
        cmd[4] = count
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum
        print("led yellow", [hex(x) for x in cmd], sum, btype, count)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    def ledRed(self, btype=1, count=1):
        if btype < 0:
            btype = 1  # OFF
        if btype >= 4:
            btype = 4
        if count <= 0:
            count = 1
        if count > 10:
            count = 10

        cmd = self.CMD_DEVICE
        cmd[2] = 0x3
        cmd[3] = btype
        cmd[4] = count
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum
        print("led red", [hex(x) for x in cmd], sum, btype, count)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        return ret

    CMD_CLEAR_ALL_ROUTE = list(b'\xf0\x10\x00\x00')

    # 清楚所有路徑....需等到清除完成回應, 約30秒, 機器會動....
    def clearAllRoute(self):
        cmd = self.CMD_CLEAR_ALL_ROUTE
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("clearAllRoute", [hex(x) for x in cmd], sum)
        self.ser.write(cmd)
        ret = self.waitResponse(4, 300)
        return ret

    routePoints = -1
    CMD_START_TRAIN = list(b'\xf0\x30\x00\x00')

    # 開啟訓練模式.. 路徑 0~5
    def startTrain(self, routeId=0):
        cmd = self.CMD_START_TRAIN
        if routeId < 0:
            routeId = 0
        if routeId > 5:
            routeId = 5
        cmd[2] = routeId
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("startTrain", [hex(x) for x in cmd], sum, routeId)
        self.ser.write(cmd)
        ret = self.waitResponse(4, 300)
        self.routePoints = 0
        return ret

    CMD_SETPOINT = list(b'\xf0\x31\x00\x00\x00')

    # 設定訓練點,
    # passval.....無關
    # direction=1 :以向前走方式移動後設定的點
    # direction=1 :以向後走方式移動後設定的點
    def setRoutePoint(self, passval=0, direction=0):
        cmd = self.CMD_SETPOINT
        self.CMD_SETPOINT[2] = passval
        self.CMD_SETPOINT[3] = direction
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("setRoutePoint", [hex(x) for x in cmd], sum, passval, direction)
        self.ser.write(cmd)
        # ret = self.ser.read(5)
        ret = self.waitResponse(5, 300)
        self.routePoints = self.routePoints+1
        return ret

    CMD_SAVE_TRAIN = list(b'\xf0\x32\x00\x00')

    # 完成訓練, 儲存...
    def saveRoute(self):
        cmd = self.CMD_SAVE_TRAIN
        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("setRoutePoint", [hex(x) for x in cmd], sum, self.routePoints)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        self.routePoints = -1
        return ret

    currentRoute = -1

    CMD_RUN_ROUTE = list(b'\xf0\x50\x00\x00\x00')

    # 執行特定路徑, 並移動到特定點... 測試結果是: 自行執行前需先停止行進(INTERRUPT)
    def runRoute(self, routeId, startPoint=0):
        cmd = self.CMD_RUN_ROUTE
        if routeId < 0:
            routeId = 0
        if routeId > 5:
            routeId = 5
        cmd[2] = routeId
        cmd[3] = startPoint & 0xff

        sum = self.checkSum(cmd)
        cmd[len(cmd)-1] = sum

        print("runRoute", [hex(x) for x in cmd], sum, routeId, startPoint)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        self.currentRoute = routeId
        return ret

    CMD_INTERRUPT_ROUTE = list(b'\xf0\x51\x00\x00')

    # 取消目前行進的作業, 測試結果是: 自行執行前需先停止行進
    def interruptRoute(self, route=0):
        cmd = self.CMD_INTERRUPT_ROUTE
        sum = self.checkSum(cmd)
        cmd[2] = route
        cmd[len(cmd)-1] = sum

        print("interruptRoute", [hex(x) for x in cmd], sum)
        self.ser.write(cmd)
        # ret = self.ser.read(4)
        ret = self.waitResponse(4, 300)
        self.currentRoute = -1

        return ret

    # 監看執行狀態, 尚未測試
    def monitorRoute(self):
        errCount = 0
        while errCount < 10:
            time.sleep(3)
            ret = self.getWorkingStatus()
            if len(ret) < 4:
                errCount += 1
                continue
            if ret[2] == 0:
                continue  # None ???
            if ret[2] == 1:
                continue  # normal
            if ret[2] == 127:
                return 127
            return ret[2]-256


#
class GamePad:
    xChanged = False
    zChanged = False
    xLast = 127
    zLast = 127
    btnT1 = btnT2 = btnT3 = btnT4 = 0
    btnL1 = btnL2 = btnR1 = btnR2 = 0
    btnSelect = btnStart = 0

    def __init__(self):
        print("init GamePad")

    # 讀取下一筆...
    def readNext(self):
        events = inputs.get_gamepad()
        for e in events:
            if e.code == 'ABS_Y':
                self.xChanged = self.xLast != e.state
                self.xLast = e.state
                inputs.get_gamepad()
            if e.code == 'ABS_X':
                self.zChanged = self.zLast != e.state
                self.zLast = e.state
                inputs.get_gamepad()

            if e.code == 'MSC_SCAN' and e.state == 589825:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnT1 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589826:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnT2 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589827:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnT3 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589828:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnT4 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589829:
                v = inputs.get_gamepad()  # TN_TRIGGER
                # print(v[0].ev_type, v[0].code, v[0].state)
                self.btnL1 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589830:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnR1 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589831:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnL2 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589832:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnR2 = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589833:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnSelect = v[0].state
                inputs.get_gamepad()  # sync report

            if e.code == 'MSC_SCAN' and e.state == 589834:
                v = inputs.get_gamepad()  # TN_TRIGGER
                self.btnStart = v[0].state
                inputs.get_gamepad()  # sync report

        return events


# 將X值從(IN_MIN~ IN_MAX) 線性對應到 (OUT_MIN, OUT_MAX)
def map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

# 測試訓練用的移動...需>1M以上的移動


def trainMove(tircgo, speed=30, delay=0.5, loopCount=6):
    for c in range(0, loopCount):
        ret = tircgo.move(speed, 0)
        time.sleep(delay)
        # print("move=", ret)


# 以預計的路徑訓練, 供測試使用
def autoTrain(tircgo, route, fw):

    ret = tircgo.getAlive()
    print("getAlive=", ret)
    # time.sleep(5)

    ret = tircgo.clearAllRoute()
    print("clear route =", ret)
    time.sleep(2)

    ret = tircgo.startTrain(route)
    print("startTrain =", ret)
    time.sleep(5)

    ret = tircgo.setRoutePoint(0, fw)
    print("setRoutePoint =", ret)
    time.sleep(2)

    trainMove(tircgo, 30, 0.5, 10)
    time.sleep(3)

    ret = tircgo.setRoutePoint(0, fw)
    print("setRoutePoint =", ret)
    time.sleep(2)

    trainMove(tircgo, 30, 0.5, 10)
    time.sleep(3)

    ret = tircgo.setRoutePoint(0, fw)
    print("setRoutePoint =", ret)
    time.sleep(2)

    trainMove(tircgo, 30, 0.5, 10)
    time.sleep(3)

    ret = tircgo.setRoutePoint(0, fw)
    print("setRoutePoint =", ret)
    time.sleep(2)

    ret = tircgo.saveRoute()
    print("saveRoute =", ret)
    time.sleep(5)

    # ret=tircgo.runRoute(1,2)
    # print("setroute =", ret)


# 以GAMEPAD訓練路徑!!
# 最大速度, 角度
# 運作多少次
def trainWithGamepad(tircgo, gp, maxX=60, maxZ=10, loops=100000):
    posChanged = False
    setting = False
    running = False
    route = 0
    print("===========================================================")
    print("X,Y:", "move")
    print("1,2,3,4,R1,R2:", "route #")
    print("select,select,L1:", "start trainning, save point/node, save route")
    print("start, start:", "use route and run, or interrupt route")
    print("L2:", "reserved")
    print("===========================================================")

    # ret=tircgo.getAlive()
    # print("ret=", ret)

    while loops > 0:
        loops = loops-1
        events = gp.readNext()

        ## print("Y",gp.xChanged,gp.xLast, "X",gp.zChanged,gp.zLast)

        # print("XY",gp.zChanged,gp.zLast,gp.xChanged,gp.xLast,
        #     "btn",gp.btnT1,gp.btnT2,gp.btnT3,gp.btnT4,
        #     "front",gp.btnL1,gp.btnL2,gp.btnR1,gp.btnR2,
        #     "select",gp.btnSelect,gp.btnStart
        #     )
        print("set=", setting, "run=", running, "rout=", route, posChanged)

        # gp.xChanged=gp.zChanged=False
        # for e in events:
        #     print(e.ev_type, e.code, e.state)
        #     time.sleep(0.1)

        # X,Y 移動
        if (gp.xLast != 127 or gp.zLast != 127) and running == 0:
            # xval=map(gp.xLast,0,255,-60,60)
            # zval=map(gp.zLast,0,255,-10,10)
            if gp.xLast == 0:
                xval = maxX
            if gp.xLast == 127:
                xval = 0
            if gp.xLast == 255:
                xval = - maxX
            if gp.zLast == 0:
                zval = maxZ
            if gp.zLast == 127:
                zval = 0
            if gp.zLast == 255:
                zval = - maxZ

            ret = tircgo.move(xval, zval)
            if setting != 0:
                posChanged = True
            print("==> move", xval, zval, ret, posChanged)
            continue

        # SELECT BUTTON
        # 進行訓練, , SEETING=0 時, 傳送開始訓練, 並設SETTING=1
        if gp.btnSelect and running == 0 and setting == 0:
            setting = 1
            posChanged = False
            ret = tircgo.startTrain(route)
            print(">>> Start trainning", route, ret)
            tircgo.beep(2)
            tircgo.ledYellow()
            continue

        # SELECT BUTTON
        # 進行訓練, 未進入訓練模式, 或路徑未變更, SEETING=1 時, 傳送設定, 並累加設定點
        if gp.btnSelect and running == 0 and setting == 1 and posChanged != 0:
            setting = 1
            posChanged = False
            ret = tircgo.setRoutePoint(route)
            print(">>> save point:", route, tircgo.routePoints, ret)
            tircgo.ledGreen()
            tircgo.beep(3)
            continue

        # 左上BUTTON, 存檔....
        if gp.btnL1 and setting != 0 and running == 0:
            posChanged = False
            setting = 0
            ret = tircgo.saveRoute()
            print(">>> finish route", route, tircgo.routePoints, ret)
            tircgo.beep(4)
            tircgo.ledGreen(-1)
            tircgo.ledYellow(-1)
            tircgo.ledRed(-1)

            continue

        # START... 使用路徑.... BUT....如何設定到路徑點?
        # 所以只會移到原始點.....
        if gp.btnStart and setting == 0 and running != 0:
            running = 0
            ret = tircgo.interruptRoute()
            print("*** interrupt", route, ret)
            continue

        # START... 使用路徑.... BUT....如何設定到路徑點?
        # 所以只會移到原始點.....
        if gp.btnStart and setting == 0 and running == 0:
            running = 1
            ret = tircgo.runRoute(route)
            print("*** run route", route, ret)
            continue

        # 使用數字鍵 選用路徑
        selectedRoute = route
        if setting == 0 and running == 0:
            if gp.btnT1:
                selectedRoute = 0
            if gp.btnT2:
                selectedRoute = 1
            if gp.btnT3:
                selectedRoute = 2
            if gp.btnT4:
                selectedRoute = 3
            if gp.btnR1:
                selectedRoute = 4
            if gp.btnR2:
                selectedRoute = 5
            if selectedRoute != route:
                print("!!! change route from", route, "to", selectedRoute)
                route = selectedRoute
                continue

# ===================================================


def getCharWithSelect(Block=False, timeout=0.1):
    if Block or select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
        c = sys.stdin.read(1)
        v = ord(c)
        print("get char:", c, v)
        return v
    return 0xff

# not tested yet!!!!!!
def trainWithKeyboard(tircgo):
    route = 0
    while True:
        c = getCharWithSelect()
        if c == 'i':
            ret = tircgo.move(60, 0)
        if c == 'k':
            ret = tircgo.move(-60, 0)
        if c == 'j':
            ret = tircgo.move(-10, 0)
        if c == 'l':
            ret = tircgo.move(10, 0)

        if c >= ord('0') and c <= ord('9'):
            ret = route = c-ord('0')+1

        if c == 'b':
            ret = tircgo.startTrain(route)
        if c == 's':
            ret = tircgo.setRoutePoint(0,1)
        if c == 'S':
            ret = tircgo.setRoutePoint(0,0)
        if c == 'e':
            ret = tircgo.saveRoute()

        if c == 'r':
            ret = tircgo.runRoute(route)
        if c == 'x':
            ret = tircgo.interruptRoute()
        print("result=", ret)


# 單元測試....
def unitTest(tricgo):
    ret = tircgo.getAlive()
    ret = tircgo.getCurrentMode()
    ret = tircgo.getWorkingStatus()
    ret = tircgo.getGetLidar()
    ret = tircgo.shutdown()
    ret = tircgo.move(15, 10)
    ret = tircgo.beep(15, 10)
    ret = tircgo.ledGreen(15, 10)
    ret = tircgo.ledYellow(15, 10)
    ret = tircgo.ledRed(15, 10)

    ret = tircgo.clearAllRoute()
    ret = tircgo.startTrain(1)
    ret = tircgo.setRoutePoint(1, 1)
    ret = tircgo.saveRoute()

    ret = tircgo.runRoute(1)
    ret = tircgo.interruptRoute()
    print(ret)

# 前後移, 發聲測試


def simpleTest(tricgo):
    tricgo.tircgoMove(16, 0)
    time.sleep(2)
    tricgo.tircgoMove(-16, 0)
    time.sleep(2)
    tricgo.tircgoMove(0, 0)

    tircgo.beep(2, 5)
    time.sleep(2)


# ========================
# 主程式.....
# ========================

# 界面物件
tircgo = Tircgo()
gp = GamePad()

# 使用預設練, 做測試, 訓練第0 路徑, 向前訓練
# --------------------------
# autoTrain(tircgo,0,1)
# --------------------------

# 是否回原點.....
# --------------------------
# ret=trainMove(tircgo,speed=-30,delay=0.5, loopCount=26)
# --------------------------


# 測試路徑 VVVVVVVVVVVVVVVVVVVVVVVVVVV

# 1. 讀取目前模式
ret = tircgo.getCurrentMode()
print("run=", ret)

# 2. 停止執行路徑
ret = tircgo.interruptRoute(0)
print("interruptRoute=", ret)

# 2. 啟動路徑... EX: ROUTE:0, NODE:2
ret = tircgo.runRoute(0, 2)
print("run=", ret)

# 透過GAMEPAD測試
# trainWithGamepad(tircgo,gp)

# 執行單元測試
# unitTest(tricgo)

# 執行簡易測試
# simpleTest(tricgo)

print("done...")
