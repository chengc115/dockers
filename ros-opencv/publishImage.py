#!/usr/bin/python
# gitlab: LaneDetection_Ros; 	url = https://github.com/zhangcaocao/LaneDetection_Ros.git


import math
import sys
import time

import cv2
import numpy
import rospy
import sensor_msgs.msg
from cv_bridge import CvBridge, CvBridgeError, getCvType



QUEUE_SIZE = 10

# Send each image by iterate it from given array of files names  to a given topic,
# as a regular and compressed ROS Images msgs.
class Source:

    def __init__(self, topic, cam):
        self.pub = rospy.Publisher(topic, sensor_msgs.msg.Image, queue_size=QUEUE_SIZE)
        self.cap = cv2.VideoCapture(int(cam))
        self.topic=topic

    def spin(self):
        cvb = CvBridge()
        rate = rospy.Rate(10)
        while not rospy.core.is_shutdown():
            ret, cvim = self.cap.read()
            # print type(cvim), cvim.shape
            # print(ret)
            # cvim = cv2.imread('/home/zhangcaocao/catkin_ws/src/lane_detection/test/test2.jpg')
            # cvim = cv2.resize(cvim ,(80, 60), interpolation=cv2.INTER_CUBIC)
            cvim = cv2.resize(cvim ,(160, 120), interpolation=cv2.INTER_CUBIC)
            
            # cvim = cv2.resize(cvim ,(320, 240))
            # cv2.imshow("test",cvim)
            cv2.imshow("test",cvim)
            cv2.waitKey(1)
            rospy.loginfo("image shape: " + str(cvim.shape) +" topic="+self.topic)
            # self.pub.publish(cvb.cv2_to_imgmsg(cvim))
            self.pub.publish(cvb.cv2_to_imgmsg(cvim, "bgr8"))
            rate.sleep()

def main(args):
    s = Source('/TTT/Image', args[1])
    print cv2.__version__
    rospy.init_node('Source')
    
    try:
        s.spin()
        rospy.spin()
        outcome = 'test completed'
    except KeyboardInterrupt:
        print "shutting down"
        outcome = 'keyboard interrupt'
    rospy.core.signal_shutdown(outcome)

if __name__ == '__main__':
    print "check with : rosrun image_view image_view image:=/TTT/Image _do_dynamic_scaling:=true"

    print "topic: /TTT/Image"
    print "from web cam"
    main(sys.argv)