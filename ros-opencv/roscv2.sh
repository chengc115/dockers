#!/bin/bash

IMAGE_NAME=ros-opencv
DOCKER_NAME=roscv2
ROSWS_HOME=/home/$USER/ros_ws

docker build -t $IMAGE_NAME .

xhost +
docker stop $DOCKER_NAME
docker rm $DOCKER_NAME

docker run -it \
    -p 11411:11411 \
    -p 11311:11311 \
    --name=$DOCKER_NAME \
    --device=/dev/dri:/dev/dri  \
    --device=/dev/video0:/dev/video0 \
    --device=/dev/video1:/dev/video1 \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=${XAUTH}" \
    --volume=$XSOCK:$XSOCK \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix" \
    --volume=$ROSWS_HOME:/ros_ws \
    $IMAGE_NAME

    
#     --volume=$XSOCK:$XSOCK:rw \

# url = https://github.com/UbiquityRobotics/dnn_detect.git
# url = https://github.com/introlab/find-object.git
# url = https://github.com/ros-perception/opencv_apps.git
# https://github.com/yoshito-n-students/mjpeg_client.git
# https://github.com/brunohorta82/ESP32_CAR_CAM
# https://github.com/ros-drivers

