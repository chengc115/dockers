#!/bin/bash


AP_NAME=tomcat
AP_HOME=/home/tomcat
AP_UID=8080
AP_GID=8080
AP_PORT_LEAD=

# DOCKER_IMAGE=tomcat:8.5.38-jre8
DOCKER_IMAGE=tomcat:9.0.27-jdk12-adoptopenjdk-hotspot
DOCKER_CONTAINER_HOME=/usr/local/tomcat
DOCKER_CONTAINER_DIRS="logs temp work webapps conf"
DOCKER_UID=8080


################################################

if [ ! -d $AP_HOME/webapps ]; then
  echo Create runtime env.
  echo Make HOME_DIR = $AP_HOME
  mkdir -p $AP_HOME

  # -----------------------
  echo -n Frist time..
  docker run -d \
    --name=${AP_NAME} ${DOCKER_IMAGE}
  sleep 10
  echo -n stopping ....
  docker stop ${AP_NAME}

  # -----------------------
  echo Copy running time parameter
  for dir in $DOCKER_CONTAINER_DIRS; do
    echo docker cp ${AP_NAME}:${DOCKER_CONTAINER_HOME}/${dir} $AP_HOME
    docker cp ${AP_NAME}:${DOCKER_CONTAINER_HOME}/${dir} $AP_HOME
  done
  # ln -s $AP_HOME/webapps $AP_HOME/..


  # -----------------------
  echo -n Remove old continer:
  docker rm ${AP_NAME}

  # -----------------------
  chown -R $AP_UID:$AP_GID $AP_HOME
  echo done
fi


################################################


DOC_NAME=$(docker ps -a --format '{{.Names}}' --filter "name=${AP_NAME}")
DOC_STAT=$(docker ps -a --format '{{.Status}}' --filter "name=${AP_NAME}")

# echo current continer=$RUNNING

################################################
if [ ! -z "$DOC_NAME" ] ; then
   echo $DOC_NAME = $DOC_STAT ; NAME=$AP_NAME
   docker start $AP_NAME
else
   echo Docker container not exist, create from image=$AP_IMAGE
docker run -tid --name ${AP_NAME} \
   -p ${AP_PORT_LEAD}8080:8080 \
   -p ${AP_PORT_LEAD}80:8080 \
   -p ${AP_PORT_LEAD}443:8443 \
   -v $AP_HOME/logs:${DOCKER_CONTAINER_HOME}/logs \
   -v $AP_HOME/work:${DOCKER_CONTAINER_HOME}/work \
   -v $AP_HOME/temp:${DOCKER_CONTAINER_HOME}/temp \
   -v $AP_HOME/webapps:${DOCKER_CONTAINER_HOME}/webapps \
   ${DOCKER_IMAGE}
fi

##############################################################
echo ===================================================
docker ps -a --format '{{.Names}}\t\t{{.Status}}'
echo ===================================================


