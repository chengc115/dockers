# AP_HOME=/home/opass/tomcat
INST_NAME=tomcat
INST_HOME=/home/tomcat
INST_IMAGE=tomcat:8.5.37-jre8
mkdir -p $INST_HOME/logs
mkdir -p $INST_HOME/temp
mkdir -p $INST_HOME/work
mkdir -p $INST_HOME/webapps

#   -e MOA_COUCHDB_USERNAME=admin \
#   -e MOA_COUCHDB_PASSWORD=22927399 \

docker run -tid --restart unless-stopped \
   -p 80:8080 -p 443:8443 \
   -v $INST_HOME/logs:/usr/local/tomcat/logs \
   -v $INST_HOME/work:/usr/local/tomcat/work \
   -v $INST_HOME/temp:/usr/local/tomcat/temp \
   -v $INST_HOME/webapps:/usr/local/tomcat/webapps \
   --name=$INST_NAME  $INST_IMAGE
