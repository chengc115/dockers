#!/bin/bash

COUCHDB_DB=couchdb
# COUCHDB_USER=admin
# COUCHDB_PASSWORD=22927399
COUCHDB_USER=
COUCHDB_PASSWORD=

AP_NAME=couchdb
AP_HOME=/home/couchdb
AP_UID=5984
AP_GID=5984
AP_PORT_LEAD=

DOCKER_IMAGE=apache/couchdb:2.3.1
DOCKER_CONTAINER_HOME=/opt/couchdb
DOCKER_CONTAINER_DIRS="etc data var"
DOCKER_UID=5984


# add user/group 5984 into /etc/passwd, /etc/group


if [ ! -d $AP_HOME/couchdb ]; then
  [[ -d $AP_HOME/couchdb ]] || mkdir -p $AP_HOME
  echo Create runtime env.
  echo Make HOME_DIR = $AP_HOME
  mkdir -p $AP_HOME

  # -----------------------
  echo -n Frist time:
  docker run -d \
    --name=${AP_NAME} ${DOCKER_IMAGE}
  sleep 10
  echo -n stopping ....
  docker stop ${AP_NAME}

  # -----------------------
  echo Copy running time parameter: $DOCKER_CONTAINER_DIRS

  # docker cp ${AP_NAME}:${DOCKER_CONTAINER_HOME}/etc $AP_HOME
  for dir in $DOCKER_CONTAINER_DIRS; do
    echo docker cp ${AP_NAME}:${DOCKER_CONTAINER_HOME}/${dir} $AP_HOME
    docker cp ${AP_NAME}:${DOCKER_CONTAINER_HOME}/${dir} $AP_HOME
  done


  # -----------------------
  echo -n Remove old continer:
  docker rm ${AP_NAME}

  # -----------------------
  sudo chown -R $DOCKER_UID $AP_HOME
  echo done
fi



DOC_NAME=$(docker ps -a --format '{{.Names}}' --filter "name=${AP_NAME}")
DOC_STAT=$(docker ps -a --format '{{.Status}}' --filter "name=${AP_NAME}")

# echo current continer=$RUNNING

if [ ! -z "$DOC_NAME" ] ; then
   echo $DOC_NAME = $DOC_STAT ; NAME=$AP_NAME
   docker start $AP_NAME
else
   echo Docker container not exist, create $AP_NAME from image=$DOCKER_IMAGE
   # docker run  --name ${AP_NAME} 
############################################
#    --env COUCHDB_USER=${COUCHDB_USER} \
#    --env COUCHDB_PASSWORD=${COUCHDB_PASSWORD} \
############################################


#     -e ERL_FLAGS='-setcookie "dell" -name "dell" ' \
#    -e NODENAME=DELL \
#    -e ERLANGCOOKIE

   docker run --restart=always -tid --name ${AP_NAME} \
    -p ${AP_PORT_LEAD}5984:5984 \
    -p 5986:5986 -p 4369:4369 -p 9100:9100 \
    --volume ${AP_HOME}/data:$DOCKER_CONTAINER_HOME/data \
    --volume ${AP_HOME}/etc:$DOCKER_CONTAINER_HOME/etc \
    --volume ${AP_HOME}/var:$DOCKER_CONTAINER_HOME/var \
    ${DOCKER_IMAGE}
fi


# echo \
#    docker run --restart=always -tid --name ${AP_NAME} \
#     -p ${AP_PORT_LEAD}5984:5984 \
#     --volume ${AP_HOME}/data:$DOCKER_CONTAINER_HOME/data \
#     --volume ${AP_HOME}/etc:$DOCKER_CONTAINER_HOME/etc \
#     --env COUCHDB_USER=${COUCHDB_USER} \
#     --env COUCHDB_PASSWORD=${COUCHDB_PASSWORD} \
#     ${DOCKER_IMAGE}

# --volume ~/etc/local.d:/opt/couchdb/etc/local.d
#
# "-e COUCHDB_USER=admin -e COUCHDB_PASSWORD=password"
# --volume /home/MOA/couchdb/etc/local.d:/opt/couchdb/etc/local.d \
#

echo ===================================================
docker ps -a --format '{{.Names}}\t\t{{.Status}}'
docker logs -f ${AP_NAME}
echo ===================================================

