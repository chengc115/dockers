INST_NAME=emqx
INST_HOME=/home/emqx
INST_IMAGE=emqx/emqx

# mkdir -p $INST_HOME/logs
# mkdir -p $INST_HOME/temp
# mkdir -p $INST_HOME/work
# mkdir -p $INST_HOME/webapps

# emqx:/opt/emqx/data

docker run -tid --restart unless-stopped \
   -p 1883:1883 -p 18083:18083 \
   -v $INST_HOME/log:/opt/emqx/log \
   -v $INST_HOME/data:/opt/emqx/data \
   -v $INST_HOME/etc:/opt/emqx/etc \
   --name $INST_NAME $INST_IMAGE
