
#!/usr/bin/python3

from py532lib.i2c import *
from py532lib.frame import *
from py532lib.constants import *

import roslib
import rospy
from std_msgs.msg import String
 
def main() :
    pn532 = Pn532_i2c()
    pn532.SAMconfigure()
 
    pub = rospy.Publisher('nfctag', String, queue_size=10)
    rospy.init_node('samula', anonymous=True)
    rospy.loginfo("init ....")
    rate = rospy.Rate(1) # 10hz
    last_card_data=""
    while not rospy.is_shutdown():
        
        card_data = pn532.read_mifare().get_data()
        print("take tag:",card_data)
        if len(card_data)>0 :
            hello_str = "TAG:%s" % card_data.hex()
            rospy.loginfo(hello_str)
            if True or last_card_data != card_data :
                pub.publish(hello_str)
                print("send ros:",hello_str)
                last_card_data=card_data
        else :
            print("skip....")
        rate.sleep()
    # card_data = pn532.read_mifare().get_data()
    # if len(card_data>0)
 
    print(card_data)


if __name__ == '__main__':
    try :
        main()
    except rospy.ROSInterruptException:
        pass
