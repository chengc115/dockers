
xhost +
docker stop rosmx
docker rm rosmx

docker run -it \
    -p 11411:11411 \
    -p 11311:11311 \
    --name=rosmx \
    --device=/dev/dri:/dev/dri  \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=${XAUTH}" \
    --volume=$XSOCK:$XSOCK:rw \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume=/home/$USER/ros:/opt/ros_ws \
    rosmx  roscore
    

