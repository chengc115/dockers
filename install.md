# install

## base

sudo apt-get install git cmake

git clone https://github.com/dusty-nv/jetson-inference

cd jetson-inference

git submodule update --init

https://blog.cavedu.com/2019/04/30/nvidia-jetson-nano-example/



## setup Jetson Nano

### setup Wifi

more /etc/network/interfaces

``` config
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d


auto lo

iface lo inet loopback
# iface eth0 inet dhcp

allow-hotplug wlan0
auto wlan0
#
iface wlan0 inet manual
wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
iface default inet dhcp

```

 more /etc/wpa_supplicant/wpa_supplicant.conf 
 
```
 country=TW
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

# Simple case: WPA-PSK, PSK as an ASCII passphrase, allow all valid ciphers

network={
	ssid="chengc-GF62-8RE"
	psk="12345678"
	key_mgmt=WPA-PSK
# 	priority=10
# 	scan_ssid=1
}

...
```


## setup ROS

### torch-1.3.0-cp36-cp36m-linux_aarch64.whl


### SSH
ssh -X username@ipaddress

### PN532

pip3 install pn532lib


+ https://osoyoo.com/zh/2017/07/20/pn532-nfc-rfid-module-for-raspberry-pi/

apt-get  install  libusb-dev  libpcsclite-dev  i2c-tools  -y

nfc-scan-device -i -v

nfc-list, nfc-poll

sudo   mkdir   /etc/nfc
sudo   nano   libnfc.conf
i2cdetect –y 1 : ID=24




```
device.name = "PN532 board via UART"
allow_intrusive_scan = true
device.connstring = pn532_i2c:/dev/i2c-1
```

+ PN532 PIN, UPPER to right, Bottom:to left(off)
+ 


### USBCAM

git clone https://github.com/ros-drivers/usb_cam.git

catkin_make

roslaunch usb_cam usb_cam-test.launch 


+ rosrun usb_cam usb_cam_node 
+ topic:/usb_cam/image_raw
+ more src/usb_cam/launch/usb_cam-test.launch